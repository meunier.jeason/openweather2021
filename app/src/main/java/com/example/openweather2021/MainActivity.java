package com.example.openweather2021;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {
    private List<Weather> weatherList = new ArrayList<Weather>();
    private WeatherAdapter weatherAdapter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        weatherAdapter = new WeatherAdapter(weatherList);

    }


    public void findWeather(View view) {
        EditText editText = findViewById(R.id.editTextVille);
        view.setEnabled(false);
        URL url = createURL(editText.getText().toString());
        if(url != null){
            GetWeatherTask getWeatherTask = new GetWeatherTask(this);
            getWeatherTask.execute(url);
        } else{
            Toast.makeText(this,"URL invalide",Toast.LENGTH_SHORT).show();
        }
    }

    private URL createURL(String city) {
        String apiKey = getString(R.string.key);
        String baseUrl = getString(R.string.web_service_url);

        try{
            String urlString = baseUrl + URLEncoder.encode(city,"UTF-8")+"&units=metric&cnt=16&APPID="+apiKey;
            Log.d("MesLogs",urlString);
            return new URL(urlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    private class GetWeatherTask extends AsyncTask<URL,Void, JSONObject>{
        private Context context;

        public GetWeatherTask(Context c){
            context = c;
        }

        @Override
        protected JSONObject doInBackground(URL... params) {
            HttpsURLConnection connection = null;
            try {
                connection = (HttpsURLConnection)params[0].openConnection();
                connection.setConnectTimeout(5000);
                int reponse = connection.getResponseCode();
                if(reponse == HttpsURLConnection.HTTP_OK){
                    StringBuilder builder = new StringBuilder();
                    try(BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream())
                    )){
                        String line;
                        while ((line= reader.readLine()) != null){
                            builder.append(line);
                        }
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                    return new JSONObject(builder.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject weather){
            TextView textView = findViewById(R.id.textViewResultat);
            Button b = findViewById(R.id.button);
            b.setEnabled(true);
            if(weather != null){
                convertJSONtoArrayList(weather);
                textView.setText("min: "+weatherList.get(0).getMinTemp()+" max = "+weatherList.get(0).getMaxTemp());
                Toast.makeText(getApplicationContext(), "Maj Effectuée",Toast.LENGTH_LONG).show();
            }
        }
    }

    private void convertJSONtoArrayList(JSONObject object){
        weatherList.clear();
        try {
            JSONArray list = object.getJSONArray("list");
            for (int i=0; i<list.length();i++){
                JSONObject day = list.getJSONObject(i);
                JSONObject temperature = day.getJSONObject("temp");
                JSONObject weather = day.getJSONArray("weather").getJSONObject(0);
                weatherList.add(new Weather(
                        day.getLong("dt"),
                        temperature.getDouble("min"),
                        temperature.getDouble("max"),
                        day.getDouble("humidity"),
                        weather.getString("description"),
                        weather.getString("icon")
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}

