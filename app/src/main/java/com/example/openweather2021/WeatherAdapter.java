package com.example.openweather2021;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.MyViewHolder> {
    private static List<Weather> weatherList;

    private static Map<String,Bitmap> bitmaps = new HashMap<>();

    public WeatherAdapter(List<Weather> list){
        weatherList=list;
    }

    public static void setWeatherList(List<Weather> list){
        WeatherAdapter.weatherList=list;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView tmin;
        private TextView tmax;
        private TextView thumidity;
        private TextView tday;
        private ImageView imageView;

        public MyViewHolder(final View view){
            super(view);
            tmin = view.findViewById(R.id.tvLow);
            tmax = view.findViewById(R.id.tvHigh);
            thumidity = view.findViewById(R.id.tvHumidity);
            tday = view.findViewById(R.id.tvDay);
            imageView = view.findViewById(R.id.imageView);
        }

        public void display(Weather weather){
            tmin.setText(weather.getMinTemp());
            tmax.setText(weather.getMaxTemp());
            thumidity.setText(weather.getHumidity());
            tday.setText(weather.getDayOfWeek());
            if (bitmaps.containsKey(weather.getIconURL())){
                imageView.setImageBitmap(bitmaps.get(weather.getIconURL()));
            }
            else {
                new LoadImageTask(imageView).execute(weather.getIconURL());
            }
        }

        private class LoadImageTask extends AsyncTask<String,Void,Bitmap>{
            private ImageView imageView;

            public LoadImageTask(ImageView i){
                imageView = i;
            }

            @Override
            protected Bitmap doInBackground(String... params){
                Bitmap bitmap = null;
                HttpURLConnection connection = null;

                try {
                    URL url = new URL(params[0]);
                    connection = (HttpURLConnection)url.openConnection();
                    connection.setConnectTimeout(5000);
                    int reponse = connection.getResponseCode();
                    Log.d("mestests","reponse : "+reponse);
                    if (reponse == HttpURLConnection.HTTP_OK) {
                        try (InputStream inputStream = connection.getInputStream()) {
                            bitmap = BitmapFactory.decodeStream(inputStream);
                            bitmaps.put(params[0], bitmap);
                        } catch (Exception e) {
                            e.printStackTrace();
                            ;
                        }
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    connection.disconnect();
                }
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap){
                imageView.setImageBitmap(bitmap);
            }
        }

    }

    @Override
    public int getItemCount(){
        return weatherList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,int viewType){
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,int position){
        Weather weather = weatherList.get(position);
        holder.display(weather);
    }


}
